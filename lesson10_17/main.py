from pygraph.graph import *

windowSize(1200, 800)
canvasSize(1200, 800)


def roof():
    brushColor("firebrick")
    polygon([(200, 300), (605, 100), (1000, 300)])


def wall(x, y, w, h):
    penSize(4)
    rectangle(x, y, x+w, y+h)


def chimney():
    pass


def door():
    pass


def wall_window(x=750, y=450, w=100, h=150):
    penSize(4)
    brushColor(50, 50, 100)
    rectangle(x, y, x+w, y+h)
    # vertical
    line(x+w/2, y, x+w/2, y+h)
    # horizontal
    line(x, y+h/2, x+w, y+h/2)



def roof_window():
    pass


def bush():
    pass


wall(250, 300, 700, 450)
roof()
wall_window()

run()
